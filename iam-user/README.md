## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| enabled | Whether to create the IAM user | string | `"true"` | no |
| force\_destroy | When destroying this user, destroy even if it has non-Terraform-managed IAM access keys, login profile or MFA devices. Without force_destroy a user with non-Terraform-managed access keys and login profile will fail to be destroyed. | string | `"false"` | no |
| groups | List of IAM user groups this user should belong to in the account | list | `<list>` | no |
| login\_profile\_enabled | Whether to create IAM user login profile | string | `"true"` | no |
| password\_length | The length of the generated password | string | `"24"` | no |
| password\_reset\_required | Whether the user should be forced to reset the generated password on first login. | string | `"true"` | no |
| path | Desired path for the IAM user | string | `"/"` | no |
| permissions\_boundary | The ARN of the policy that is used to set the permissions boundary for the user | string | `""` | no |
| pgp\_key | Provide a base-64 encoded PGP public key, or a keybase username in the form `keybase:username`. Required to encrypt password. | string | n/a | yes |
| signin\_url | IAM users sign-in URL | string | n/a | yes |
| username | Desired name for the IAM user. We recommend using an email addresses. | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| gpg\_password\_decrypt\_command | Command to decrypt the gpg encrypted password |
| gpg\_password\_pgp\_message | PGP encrypted message (e.g. suitable for email exchanges) |
| pgp\_key | PGP key used to encrypt sensitive data for this user |
| user\_arn | The ARN assigned by AWS for this user |
| user\_login\_profile\_encrypted\_password | The encrypted password, base64 encoded |
| user\_login\_profile\_key\_fingerprint | The fingerprint of the PGP key used to encrypt the password |
| user\_name | IAM user name |
| user\_unique\_id | The unique ID assigned by AWS |
| welcome\_message | Welcome message with encrypted password and login instructions |

