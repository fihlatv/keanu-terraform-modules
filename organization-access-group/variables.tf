variable "member_account_id" {
  type        = string
  description = "The ID of the member account to grant access permissions to the users in the Group"
}

variable "role_name" {
  type        = string
  default     = "OrganizationAccountAccessRole"
  description = "The name of the Role in the member account to grant permissions to the users in the Group"
}

variable "enabled" {
  type        = bool
  description = "Whether to create these resources"
  default     = true
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "switchrole_url" {
  type        = string
  description = "URL to the IAM console to switch to a role"
  default     = "https://signin.aws.amazon.com/switchrole?account=%s&roleName=%s&displayName=%s"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}
