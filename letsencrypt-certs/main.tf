/*
NOTE : the staging/production items here refer to the acme / lets encrypt
staging/production systems, and not our chatserver environments.
*/
provider "acme" {
  alias      = "staging"
  server_url = var.acme_account_staging_server_url
}

provider "acme" {
  alias      = "production"
  server_url = var.acme_account_production_server_url
}

resource "tls_cert_request" "main_staging" {
  count           = var.use_production == "1" ? 0 : 1
  key_algorithm   = var.tls_staging_private_key_algo
  private_key_pem = var.tls_staging_private_key

  subject {
    common_name = var.common_name
  }

  dns_names = var.dns_names
}

resource "tls_cert_request" "main_production" {
  count           = var.use_production == "1" ? 1 : 0
  key_algorithm   = var.tls_private_key_algo
  private_key_pem = var.tls_private_key

  subject {
    common_name = var.common_name
  }

  dns_names = var.dns_names
}

resource "acme_certificate" "main_staging" {
  count                   = var.use_production == "1" ? 0 : 1
  provider                = acme.staging
  account_key_pem         = var.acme_account_staging_key_pem
  certificate_request_pem = tls_cert_request.main_staging[0].cert_request_pem
  min_days_remaining      = var.force_renewal == "1" ? 999 : var.min_days_remaining

  dns_challenge {
    provider = "cloudflare"

    config = {
      CF_DNS_API_TOKEN  = var.cloudflare_dns_api_token
      CF_ZONE_API_TOKEN = var.cloudflare_zone_api_token
    }
  }
}

resource "acme_certificate" "main_production" {
  count                   = var.use_production == "1" ? 1 : 0
  provider                = acme.production
  account_key_pem         = var.acme_account_key_pem
  certificate_request_pem = tls_cert_request.main_production[0].cert_request_pem
  min_days_remaining      = var.force_renewal == "1" ? 999 : var.min_days_remaining

  dns_challenge {
    provider = "cloudflare"

    config = {
      CF_DNS_API_TOKEN  = var.cloudflare_dns_api_token
      CF_ZONE_API_TOKEN = var.cloudflare_zone_api_token
    }
  }
}
