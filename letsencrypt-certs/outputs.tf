output "certificate_pem" {
  value = coalesce(
    join(",", acme_certificate.main_staging.*.certificate_pem),
    join(",", acme_certificate.main_production.*.certificate_pem),
  )
}

output "private_key_pem" {
  value = var.use_production == "1" ? var.tls_private_key : var.tls_staging_private_key
}

output "issuer_pem" {
  value = coalesce(
    join(",", acme_certificate.main_staging.*.issuer_pem),
    join(",", acme_certificate.main_production.*.issuer_pem),
  )
}
