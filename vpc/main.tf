##
## VPC
## This tf creates a vpc with several subnets:
##   public_1 - public subnet
##   private_app_1 - private subnet
##   private_rds_a_1 - private subnet for RDS instances
##   private_rds_a_2 - private subnet for RDS instances, in a different AZ
##
## It sets up a NAT Gateway so instances in private_app_1 are able to access
## the internet.

module "label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = var.attributes
  tags        = var.tags
}

module "public_subnet_label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = ["public"]
  tags = merge(
    var.tags,
    {
      "Visibility" = "Public"
    },
  )
}

module "private_subnet_label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = ["private"]
  tags = merge(
    var.tags,
    {
      "Visibility" = "Private"
    },
  )
}

module "rds_subnet_label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = ["rds"]
  tags = merge(
    var.tags,
    {
      "Visibility" = "Private"
    },
  )
}

resource "aws_vpc" "default" {
  cidr_block                       = var.cidr_block
  assign_generated_ipv6_cidr_block = true
  enable_dns_hostnames             = true
  enable_dns_support               = true
  tags                             = module.label.tags
}

# seal off the default security group by adopting it and removing
# all ingress/egress rules
# we don't actually use this security group, but since it is the default,
# we want it to be secure.
resource "aws_default_security_group" "protected_default" {
  vpc_id = aws_vpc.default.id
  tags = merge(
    module.label.tags,
    {
      "Name" = format("%s%s%s", module.label.id, var.delimiter, "protecteddefault")
    },
  )
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id
  tags   = module.label.tags
}

# public subnet #1 for NAT gw
resource "aws_subnet" "public_1" {
  cidr_block              = var.subnet_public_1
  vpc_id                  = aws_vpc.default.id
  availability_zone       = var.availability_zone_1
  map_public_ip_on_launch = true
  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = format("%s%s%s", module.public_subnet_label.id, var.delimiter, "1")
    },
  )
}

# public subnet #2 in availability zone 2
resource "aws_subnet" "public_2" {
  cidr_block              = var.subnet_public_2
  vpc_id                  = aws_vpc.default.id
  availability_zone       = var.availability_zone_2
  map_public_ip_on_launch = true

  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = format("%s%s%s", module.public_subnet_label.id, var.delimiter, "2")
    },
  )
}

# private subnet #1 for RDS
resource "aws_subnet" "private_rds_a_1" {
  cidr_block        = var.subnet_rds_a_1
  vpc_id            = aws_vpc.default.id
  availability_zone = var.availability_zone_1
  tags = merge(
    module.rds_subnet_label.tags,
    {
      "Name" = format("%s%s%s", module.rds_subnet_label.id, var.delimiter, "1")
    },
  )
}

# private subnet #2 for RDS
resource "aws_subnet" "private_rds_a_2" {
  cidr_block        = var.subnet_rds_a_2
  vpc_id            = aws_vpc.default.id
  availability_zone = var.availability_zone_2
  tags = merge(
    module.rds_subnet_label.tags,
    {
      "Name" = format("%s%s%s", module.rds_subnet_label.id, var.delimiter, "2")
    },
  )
}

# private subnet #1
resource "aws_subnet" "private_app_1" {
  cidr_block        = var.subnet_private_1
  vpc_id            = aws_vpc.default.id
  availability_zone = var.availability_zone_1

  tags = merge(
    module.private_subnet_label.tags,
    {
      "Name" = format("%s%s%s", module.private_subnet_label.id, var.delimiter, "1")
    },
  )
}

resource "aws_db_subnet_group" "rds_subnets" {
  name       = "rds"
  subnet_ids = [aws_subnet.private_rds_a_1.id, aws_subnet.private_rds_a_2.id]
  tags = merge(
    module.rds_subnet_label.tags,
    {
      "Name" = module.rds_subnet_label.id
    },
  )
}

# allow internet access to nat #1
resource "aws_route_table" "public_1_to_internet" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }

  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = module.public_subnet_label.id
    },
  )
}

resource "aws_route_table_association" "internet_for_public_1" {
  route_table_id = aws_route_table.public_1_to_internet.id
  subnet_id      = aws_subnet.public_1.id
}

resource "aws_route_table" "public_2_to_internet" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }

  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = module.public_subnet_label.id
    },
  )
}

resource "aws_route_table_association" "internet_for_public_2" {
  route_table_id = aws_route_table.public_2_to_internet.id
  subnet_id      = aws_subnet.public_2.id
}

# Nat gateway #1
resource "aws_eip" "nat_1" {
  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = module.public_subnet_label.id
    },
  )
}

resource "aws_nat_gateway" "nat_1" {
  allocation_id = aws_eip.nat_1.id
  subnet_id     = aws_subnet.public_1.id
  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = module.public_subnet_label.id
    },
  )
}

# allow internet access to Application nodes through nat #1
resource "aws_route_table" "nat_gw_1" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_1.id
  }

  tags = merge(
    module.public_subnet_label.tags,
    {
      "Name" = format(
        "%s%s%s",
        module.public_subnet_label.id,
        var.delimiter,
        "natgw",
      )
    },
  )
}

resource "aws_route_table_association" "app_1_subnet_to_nat_gw" {
  route_table_id = aws_route_table.nat_gw_1.id
  subnet_id      = aws_subnet.private_app_1.id
}

# security group for our private app nodes
resource "aws_security_group" "private_app_1" {
  vpc_id = aws_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.default.cidr_block]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.default.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    module.private_subnet_label.tags,
    {
      "Name" = module.private_subnet_label.id
    },
  )
}
