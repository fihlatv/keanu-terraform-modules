## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allow\_users\_to\_change\_password | Whether to allow users to change their own password | string | `"true"` | no |
| attributes | Additional attributes (e.g. `1`, `a`, etc) | list | `<list>` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name` and `attributes` | string | `"-"` | no |
| enabled | Whether or not to create the IAM account alias | string | `"true"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`) | string | `""` | no |
| hard\_expiry | Whether users are prevented from setting a new password after their password has expired (i.e. require administrator reset) | string | `"false"` | no |
| max\_password\_age | The number of days that a user's password is valid | string | `"0"` | no |
| minimum\_password\_length | Minimum length to require for user passwords | string | `"20"` | no |
| name | The Name of the account or solution  (e.g. `account`) | string | `"account"` | no |
| namespace | Namespace (e.g. `eg` or `keanu`) | string | `""` | no |
| password\_policy\_enabled | Whether or not to create the IAM account password policy | string | `"true"` | no |
| password\_reuse\_prevention | The number of previous passwords that users are prevented from reusing | string | `"true"` | no |
| require\_lowercase\_characters | Whether to require lowercase characters for user passwords | string | `"false"` | no |
| require\_numbers | Whether to require numbers for user passwords | string | `"false"` | no |
| require\_symbols | Whether to require symbols for user passwords | string | `"false"` | no |
| require\_uppercase\_characters | Whether to require uppercase characters for user passwords | string | `"false"` | no |
| tags | Additional tags (e.g. `{"BusinessUnit" = "XYZ"}`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| account\_alias | IAM account alias |
| signin\_url | IAM users sign-in URL |

