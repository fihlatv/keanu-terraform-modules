/**
 * # Prometheus Server
 * 
 * Creates an EC2 Auto-Scaling-Group of size 1 deploying the Prometheus AMI, an
 * EBS volume for storage, and the appropriate IAM policies/roles.
 *
 */

module "label" {
  source      = "../null-label"
  namespace   = "${var.namespace}"
  environment = "${var.environment}"
  name        = "prometheus"
  delimiter   = "${var.delimiter}"
  attributes  = ["${compact(concat(var.attributes, list(var.az)))}"]
  tags        = "${var.tags}"
}

module "prometheus-data" {
  source      = "../persistent-ebs"
  namespace   = "${module.label.namespace}"
  environment = "${module.label.environment}"
  name        = "${module.label.name}"
  attributes  = "${module.label.attributes}"
  region      = "${var.region}"
  az          = "${var.region}${var.az}"
  size        = "${var.data_volume_size}"
  iops        = "${var.data_volume_iops}"
  volume_type = "${var.data_volume_type}"
  encrypted   = "${var.data_volume_encrypted}"
  kms_key_id  = "${var.data_volume_kms_key_id}"
  snapshot_id = "${var.data_volume_snapshot_id}"
}

resource "aws_security_group" "prometheus" {
  vpc_id = "${var.vpc_id}"

  # prometheus
  ingress {
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }

  # alert manager
  ingress {
    from_port   = 9093
    to_port     = 9093
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }

  # grafana
  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${module.label.tags}"
}


module "prometheus-server" {
  source             = "../asg"
  namespace   = "${module.label.namespace}"
  environment = "${module.label.environment}"
  name        = "${module.label.name}"
  attributes  = "${module.label.attributes}"
  security_group_ids = "${concat(var.security_group_ids, [aws_security_group.prometheus.id])}"
  instance_type      = "${var.instance_type}"
  ami                = "${var.ami}"
  subnet_ids         = ["${var.subnet_id}"]
  azs                = ["${var.region}${var.az}"]
  public_ip          = "${var.public_ip}"
  key_name           = "${var.key_name}"
  elb_names          = ["${var.load_balancers}"]
  max_nodes          = 1
  min_nodes          = 1
  root_volume_type   = "${var.root_volume_type}"
  root_volume_size   = "${var.root_volume_size}"

  #
  iam_profile = "${module.prometheus-data.iam_profile_id}"

  user_data = <<END_INIT
#!/bin/bash
${var.init_prefix}
${module.init-attach-ebs.init_snippet}
${var.init_suffix}
END_INIT
}

module "init-attach-ebs" {
  source    = "../init-snippet-attach-ebs-volume"
  volume_id = "${module.prometheus-data.volume_id}"
  region    = "${var.region}"
}
