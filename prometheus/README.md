# Prometheus Server

Creates an EC2 Auto-Scaling-Group of size 1 deploying the Prometheus AMI, an
EBS volume for storage, and the appropriate IAM policies/roles.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ami | The base AMI for each AWS instance created | string | n/a | yes |
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| az | The AWS Availability Zone to deploy to | string | n/a | yes |
| data\_volume\_encrypted | Boolean, whether or not to encrypt the EBS block device | string | `"true"` | no |
| data\_volume\_iops | The amount of IOPS to provision for the EBS block device | string | `""` | no |
| data\_volume\_kms\_key\_id | ID of the KMS key to use when encyprting the EBS block device | string | `""` | no |
| data\_volume\_size | Size (in GB) of EBS volume to use for the EBS volume | string | `"256"` | no |
| data\_volume\_snapshot\_id | The ID of the snapshot to base the EBS block device on | string | `""` | no |
| data\_volume\_type | Type of EBS volume to use for the EBS volume | string | `"gp2"` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| init\_prefix | init shell to run before attaching the EBS volume | string | `""` | no |
| init\_suffix | init shell to run after attaching the EBS volume | string | `""` | no |
| instance\_type | The type of AWS instance (size) | string | n/a | yes |
| key\_file | Path to the SSH private key to provide connection info as output | string | n/a | yes |
| key\_name | The name of the (AWS) SSH key to associate with the instance | string | n/a | yes |
| load\_balancers | The list of load balancers names to pass to the ASG module | list | `<list>` | no |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| prometheus\_pillar | YAML to pass to prometheus saltstack formula (pillar) | string | `""` | no |
| public\_ip | Boolean flag to enable/disable `map_public_ip_on_launch` in the launch configuration | string | `"true"` | no |
| region | The AWS region to deploy to | string | n/a | yes |
| root\_volume\_size | Size (in GB) of EBS volume to use for the root block device | string | `"30"` | no |
| root\_volume\_type | Type of EBS volume to use for the root block device | string | `"gp2"` | no |
| security\_group\_ids | list of security groups (by ID) to associate with the ASG | list | n/a | yes |
| subnet\_id | The ID of the subnet to use, depends on the availability zone | string | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| asg\_name | `name` exported from the Prometheus Server `aws_autoscaling_group` |
| key\_file | Export the `key_file` variable for convenience |

