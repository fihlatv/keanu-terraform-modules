"""
This program expects a single file input as the first positional argument.

It should be a yaml containing a list of objects with the keys:
 package, platform, key_file

It will generate a mixed yaml/json payload usable by the terraform module.

example:
---
- package: com.example.release
  platform: prod
  pem_file: com.example.release.pem

- package: com.example.dev
  platform: sandbox
  pem_file: com.example.dev.pem
"""
import sys
import os
import getpass
import json
import yaml
import textwrap


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


try:
    from OpenSSL import crypto
except ModuleNotFoundError:
    eprint("pyopenssl not found. Try: apt install python3-openssl")
    sys.exit(3)


def usage():
    print("usage: {} packages.yml".format(sys.argv[0]))
    sys.exit(1)


def p12_to_pem(p12path, password):
    if not os.path.exists(p12path):
        eprint("Supplied p12 doesn't exist: {}".format(p12path))
        sys.exit(2)

    p12_bytes = open(p12path, 'rb').read()
    try:
        p12 = crypto.load_pkcs12(p12_bytes, password)
    except Exception:
        eprint("Invalid password or corrupted p12 file.")
        sys.exit(4)

    pkey = crypto.dump_privatekey(crypto.FILETYPE_PEM, p12.get_privatekey())
    cert = crypto.dump_certificate(crypto.FILETYPE_PEM, p12.get_certificate())
    pem = "{}{}".format(cert.decode('ascii'), pkey.decode('ascii'))
    return pem


def encode_package(number, package, platform, pem):
    j = json.dumps({
        "package": package,
        "platform": platform,
        "key": pem}, indent=2)

    j = textwrap.indent(j, "    ")

    payload = "\"{}\": |\n{}".format(number, j)
    payload = textwrap.indent(payload, "    ")
    return payload


if len(sys.argv) != 2 or sys.argv[1] in ["-h", "--help"]:
    usage()


if not sys.stdin.isatty():
    password = sys.stdin.read()
else:
    password = getpass.getpass()
password = bytes(password.strip(), 'utf-8')
packages_file = sys.argv[1]

packages = yaml.load(open(packages_file, "r"))

out = "---\napps:\n"
for i, bundle in enumerate(packages):
    p12 = bundle["p12_file"]
    pem = p12_to_pem(p12, password)
    encoded = encode_package(i, bundle["package"], bundle["platform"], pem)
    out += encoded + "\n"

# sanity check: parse the generated yaml to ensure its valid
yaml.load(out)
print(out)
