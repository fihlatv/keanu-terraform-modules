#!/bin/bash
#
# Update terraform docs across all modules
#
if [[ ! $(which terraform-docs) ]]; then
  echo "Must install terraform-docs"
  exit 1
fi

for i in $(find ./ -name main.tf |sed 's/\/main.tf//g'); do
  terraform-docs md $i > $i/README.md
done
