import json
import sys

v = sys.stdin.read()
if len(v) == 0:
    print("usage: {} < path/to/cert-and-key.pem".format(sys.argv[0]))
    sys.exit(1)
else:
    j = json.dumps({"key": v})
    print(j)

