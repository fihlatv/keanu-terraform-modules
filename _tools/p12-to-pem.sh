#!/bin/bash

openssl pkcs12 -in "$1" -nodes | sed -ne '/BEGIN\/,/END\ /p' > out.pem
