terraform {
  required_version = ">= 0.12"
  required_providers {
    aws        = "~> 2.0"
    cloudflare = "~> 2.0"
    gitlab     = "~> 2.0"
  }
}
