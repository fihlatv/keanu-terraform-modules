output "deployer_username" {
  value = aws_iam_user.deploy_user.name
}

output "deployer_aws_access_key_id" {
  value = aws_iam_access_key.deploy_user_key_v3.id
}

output "deployer_aws_secret_access_key" {
  value = aws_iam_access_key.deploy_user_key_v3.secret
}

output "bucket_id" {
  value = aws_s3_bucket.origin.id
}

output "gitlab_project" {
  value = var.gitlab_project
}

output "cloudfront_distribution_id" {
  value = aws_cloudfront_distribution.site.id
}
