module "label_synapse" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = ["synapse"]
  tags        = var.tags

  additional_tag_map = {
    Application = "synapse"
  }
}

module "kms_key_synapse" {
  source      = "../kms-key"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  attributes  = ["synapse"]
  tags        = module.label_synapse.tags

  description             = "KMS key for synapse"
  deletion_window_in_days = 10
  enable_key_rotation     = "true"
  alias                   = "alias/synapse_kms_key"
}

module "ssm_prefix_synapse" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/1.1.0"
  path_prefix       = "${local.param_module_prefix}/synapse"
  prefix_with_label = false
  name              = var.name
  namespace         = var.namespace
  environment       = var.environment
  attributes        = module.label_synapse.attributes
  region            = var.region
  kms_key_arn       = module.kms_key_synapse.key_arn
}

module "synapse_session_manager" {
  source         = "git::https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy?ref=tags/0.2.0"
  name           = var.name
  namespace      = var.namespace
  environment    = var.environment
  attributes     = ["synapse"]
  s3_bucket_name = var.session_manager_bucket
  s3_key_prefix  = module.label_synapse.id
}

module "synapse_instance_role_attachment" {
  source      = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment?ref=tags/1.0.0"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  attributes  = ["synapse"]

  iam_policy_arns = [
    module.ssm_prefix_synapse.policy_arn,
    module.synapse_session_manager.ec2_session_manager_policy_arn,
  ]
}

resource "aws_iam_instance_profile" "synapse_profile" {
  name = module.label_synapse.id
  role = module.synapse_instance_role_attachment.instance_role_id
}

resource "aws_instance" "synapse" {
  ami           = var.ami_synapse
  instance_type = var.instance_type_synapse
  subnet_id     = var.subnet_id
  key_name      = var.key_name

  vpc_security_group_ids = [
    var.security_group_id_general,
    aws_security_group.synapse.id,
  ]

  iam_instance_profile = aws_iam_instance_profile.synapse_profile.id

  tags = module.label_synapse.tags

  user_data = <<EOF
#cloud-config

bootcmd:
  - 'while [ `lsblk | wc -l` -lt 4 ]; do echo waiting on disks...; sleep 5; done; lsblk; sleep 5; lsblk; lsblk --pairs --output NAME,TYPE,FSTYPE,LABEL /dev/nvme1n1;'

mounts:
  - [ "LABEL=synapse_state", "/var/lib/matrix-synapse" ]

disk_setup:
  /dev/nvme1n1:
    layout:
      - 100
    overwrite: false
    table_type: 'gpt'

fs_setup:
  -   device: /dev/nvme1n1
      partition: any
      label: synapse_state
      filesystem: ext4
      overwrite: false

runcmd:
  - 'chown -R matrix-synapse:nogroup /var/lib/matrix-synapse'
EOF

}

resource "aws_volume_attachment" "synapse_state" {
  device_name  = "/dev/sdh"
  volume_id    = aws_ebs_volume.synapse_state.id
  instance_id  = aws_instance.synapse.id
  skip_destroy = true
}

resource "aws_ebs_volume" "synapse_state" {
  availability_zone = var.availability_zone_1
  size              = var.synapse_disk_allocation_gb
  encrypted         = true
  kms_key_id        = module.kms_key_synapse.key_arn

  tags = module.label_synapse.tags
}

resource "aws_security_group" "synapse" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = local.synapse_federation_port
    to_port     = local.synapse_federation_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = local.synapse_client_port
    to_port     = local.synapse_client_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = var.node_exporter_port
    to_port     = var.node_exporter_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label_synapse.tags
}

locals {
  ssm_prefix_synapse = module.ssm_prefix_synapse.full_prefix

  # this variable does not affect the port the synapse instance uses itself, for
  # that you must edit and rebuild the image
  synapse_client_port = 8008

  synapse_federation_port = 8448
}

resource "aws_ssm_parameter" "synapse__server_name" {
  name      = "${local.ssm_prefix_synapse}/server_name"
  value     = var.domain_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__db_username" {
  name      = "${local.ssm_prefix_synapse}/db_username"
  value     = var.database_username
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__db_password" {
  name      = "${local.ssm_prefix_synapse}/db_password"
  value     = var.database_password
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__db_name" {
  name      = "${local.ssm_prefix_synapse}/db_name"
  value     = var.database_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__db_host" {
  name      = "${local.ssm_prefix_synapse}/db_host"
  value     = var.database_address
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__db_port" {
  name      = "${local.ssm_prefix_synapse}/db_port"
  value     = var.database_port
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__macaroon_secret_key" {
  name      = "${local.ssm_prefix_synapse}/macaroon_secret_key"
  value     = var.macaroon_secret_key
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__password_config_pepper" {
  name      = "${local.ssm_prefix_synapse}/password_config/pepper"
  value     = var.password_config_pepper
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__web_client_base_url" {
  name      = "${local.ssm_prefix_synapse}/web_client_base_url"
  value     = var.web_client_base_url
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__trusted_third_party_id_servers__self" {
  name      = "${local.ssm_prefix_synapse}/trusted_third_party_id_servers"
  value     = var.domain_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__shared_registration_secret" {
  name      = "${local.ssm_prefix_synapse}/shared_registration_secret"
  value     = var.shared_registration_secret
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__signing_key" {
  name      = "${local.ssm_prefix_synapse}/signing_key"
  value     = var.synapse_signing_key
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__tls_certificate" {
  name      = "${local.ssm_prefix_synapse}/tls_certificate"
  value     = var.certificate_pem
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__admin_contact_email" {
  name      = "${local.ssm_prefix_synapse}/admin_contact_email"
  value     = var.admin_contact_email
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__manhole_enabled" {
  name      = "${local.ssm_prefix_synapse}/manhole_enabled"
  value     = var.manhole_enabled
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__federation_enabled" {
  name      = "${local.ssm_prefix_synapse}/federation_enabled"
  value     = var.federation_enabled
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__metrics_enabled" {
  name      = "${local.ssm_prefix_synapse}/metrics_enabled"
  value     = var.metrics_enabled
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__metrics_bind_addresses" {
  count     = length(var.metrics_bind_addresses)
  name      = "${local.ssm_prefix_synapse}/metrics_bind_addresses/${count.index}"
  value     = element(var.metrics_bind_addresses, count.index)
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__federation_domain_whitelist" {
  count     = length(var.federation_domain_whitelist)
  name      = "${local.ssm_prefix_synapse}/federation_domain_whitelist/${count.index}"
  value     = element(var.federation_domain_whitelist, count.index)
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__from" {
  name      = "${local.ssm_prefix_synapse}/email/notif_from"
  value     = var.smtp_from
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__host" {
  name      = "${local.ssm_prefix_synapse}/email/smtp/host"
  value     = var.smtp_host
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__port" {
  name      = "${local.ssm_prefix_synapse}/email/smtp/port"
  value     = var.smtp_port
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__user" {
  name      = "${local.ssm_prefix_synapse}/email/smtp/user"
  value     = var.smtp_login
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__password" {
  name      = "${local.ssm_prefix_synapse}/email/smtp/password"
  value     = var.smtp_password
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__require_transport_security" {
  name      = "${local.ssm_prefix_synapse}/email/smtp/require_transport_security"
  value     = "true"
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__app_name" {
  name      = "${local.ssm_prefix_synapse}/email/app_name"
  value     = var.domain_name
  type      = "SecureString"
  key_id    = module.kms_key_synapse.key_arn
  overwrite = true
}
