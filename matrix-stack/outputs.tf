output "synapse_ip" {
  value = aws_instance.synapse.private_ip
}

output "synapse_id" {
  value = aws_instance.synapse.id
}

output "sygnal_ip" {
  value = aws_instance.sygnal.private_ip
}

output "ma1sd_ip" {
  value = aws_instance.ma1sd.private_ip
}

output "alb_dns_name" {
  value = aws_lb.application.dns_name
}

output "iam_certificate_name" {
  value = aws_iam_server_certificate.alb_cert_iam.name
}

output "iam_certificate_id" {
  value = aws_iam_server_certificate.alb_cert_iam.id
}

output "iam_certificate_arn" {
  value = aws_iam_server_certificate.alb_cert_iam.arn
}
