module "label_sygnal" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = ["sygnal"]
  tags        = var.tags

  additional_tag_map = {
    Application = "sygnal"
  }
}

module "kms_key_sygnal" {
  source      = "../kms-key"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  tags        = module.label_sygnal.tags
  attributes  = ["sygnal"]

  description             = "KMS key for sygnal"
  deletion_window_in_days = 10
  enable_key_rotation     = "true"
  alias                   = "alias/sygnal_kms_key"
}

module "ssm_prefix_sygnal" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/1.1.0"
  path_prefix       = "${local.param_module_prefix}/sygnal"
  prefix_with_label = false
  name              = var.name
  namespace         = var.namespace
  environment       = var.environment
  attributes        = module.label_sygnal.attributes
  region            = var.region
  kms_key_arn       = module.kms_key_sygnal.key_arn
}

module "sygnal_session_manager" {
  source         = "git::https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy?ref=tags/0.2.0"
  name           = var.name
  namespace      = var.namespace
  environment    = var.environment
  attributes     = ["sygnal"]
  s3_bucket_name = var.session_manager_bucket
  s3_key_prefix  = module.label_sygnal.id
}

module "sygnal_instance_role_attachment" {
  source      = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment?ref=tags/1.0.0"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  attributes  = ["sygnal"]

  iam_policy_arns = [
    module.ssm_prefix_sygnal.policy_arn,
    module.sygnal_session_manager.ec2_session_manager_policy_arn,
  ]
}

resource "aws_iam_instance_profile" "sygnal_profile" {
  name = module.label_sygnal.id
  role = module.sygnal_instance_role_attachment.instance_role_id
}

locals {
  ssm_prefix_sygnal = module.ssm_prefix_sygnal.full_prefix
}

resource "aws_instance" "sygnal" {
  ami           = var.ami_sygnal
  instance_type = var.instance_type_sygnal
  subnet_id     = var.subnet_id
  key_name      = var.key_name

  vpc_security_group_ids = [
    var.security_group_id_general,
    aws_security_group.sygnal.id,
  ]

  iam_instance_profile = aws_iam_instance_profile.sygnal_profile.id

  tags = module.label_sygnal.tags

  user_data = <<EOF
#cloud-config

bootcmd:
  - 'while [ `lsblk | wc -l` -lt 4 ]; do echo waiting on disks...; sleep 5; done; lsblk; sleep 5; lsblk; lsblk --pairs --output NAME,TYPE,FSTYPE,LABEL /dev/nvme1n1;'

mounts:
  - [ "LABEL=sygnal_state", "/var/lib/sygnal" ]

disk_setup:
  /dev/nvme1n1:
    layout:
      - 100
    overwrite: false
    table_type: 'gpt'

fs_setup:
  -   device: /dev/nvme1n1
      partition: any
      label: sygnal_state
      filesystem: ext4
      overwrite: false

runcmd:
  - 'chown -R sygnal:sygnal /var/lib/sygnal'

EOF

}

resource "aws_security_group" "sygnal" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = var.node_exporter_port
    to_port     = var.node_exporter_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = var.sygnal_metrics_port
    to_port     = var.sygnal_metrics_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label_sygnal.tags
}

resource "aws_volume_attachment" "sygnal_state" {
  device_name  = "/dev/sdh"
  volume_id    = aws_ebs_volume.sygnal_state.id
  instance_id  = aws_instance.sygnal.id
  skip_destroy = true
}

resource "aws_ebs_volume" "sygnal_state" {
  availability_zone = var.availability_zone_1
  size              = 2
  encrypted         = true
  kms_key_id        = module.kms_key_sygnal.key_arn
  tags              = module.label_sygnal.tags
}

resource "aws_ssm_parameter" "sygnal__apps_ios" {
  count     = length(var.sygnal_ios)
  name      = "${local.ssm_prefix_sygnal}/apps/ios/${count.index}"
  value     = element(var.sygnal_ios, count.index)
  type      = "SecureString"
  key_id    = module.kms_key_sygnal.key_arn
  tier      = length(element(var.sygnal_ios, count.index)) > 4000 ? "Advanced" : "Standard"
  overwrite = true
}

resource "aws_ssm_parameter" "sygnal__apps_android" {
  count     = length(var.sygnal_android)
  name      = "${local.ssm_prefix_sygnal}/apps/android/${count.index}"
  value     = element(var.sygnal_android, count.index)
  type      = "SecureString"
  key_id    = module.kms_key_sygnal.key_arn
  overwrite = true
}
