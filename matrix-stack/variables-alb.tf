variable "enable_deletion_protection" {
  type = string
}

variable "alb_public_subnets" {
  type = list(string)
}
