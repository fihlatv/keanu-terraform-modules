module "label_ma1sd" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = ["ma1sd"]
  tags        = var.tags

  additional_tag_map = {
    Application = "ma1sd"
  }
}

module "kms_key_ma1sd" {
  source      = "../kms-key"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  tags        = module.label_ma1sd.tags
  attributes  = ["ma1sd"]

  description             = "KMS key for ma1sd"
  deletion_window_in_days = 10
  enable_key_rotation     = "true"
  alias                   = "alias/ma1sd_kms_key"
}

module "ssm_prefix_ma1sd" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/1.1.0"
  path_prefix       = "${local.param_module_prefix}/ma1sd"
  prefix_with_label = false
  name              = var.name
  namespace         = var.namespace
  environment       = var.environment
  region            = var.region
  attributes        = module.label_ma1sd.attributes
  kms_key_arn       = module.kms_key_ma1sd.key_arn
}
module "ma1sd_session_manager" {
  source         = "git::https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy?ref=tags/0.2.0"
  name           = var.name
  namespace      = var.namespace
  environment    = var.environment
  attributes     = ["ma1sd"]
  s3_bucket_name = var.session_manager_bucket
  s3_key_prefix  = module.label_ma1sd.id
}

module "ma1sd_instance_role_attachment" {
  source      = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment?ref=tags/1.0.0"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  attributes  = ["ma1sd"]

  iam_policy_arns = [
    module.ssm_prefix_ma1sd.policy_arn,
    module.ma1sd_session_manager.ec2_session_manager_policy_arn,
  ]
}

resource "aws_iam_instance_profile" "ma1sd_profile" {
  name = module.label_ma1sd.id
  role = module.ma1sd_instance_role_attachment.instance_role_id
}

locals {
  ssm_prefix_ma1sd = module.ssm_prefix_ma1sd.full_prefix
}

resource "aws_instance" "ma1sd" {
  ami           = var.ami_ma1sd
  instance_type = var.instance_type_ma1sd
  subnet_id     = var.subnet_id
  key_name      = var.key_name

  depends_on = [
    aws_ssm_parameter.ma1sd__hostname,
    aws_ssm_parameter.ma1sd__homeserver_internal_uri,
    aws_ssm_parameter.ma1sd__synapse_db__endpoint,
    aws_ssm_parameter.ma1sd__synapse_db__name,
    aws_ssm_parameter.ma1sd__synapse_db__username,
    aws_ssm_parameter.ma1sd__synapse_db__password,
    aws_ssm_parameter.ma1sd__email__from,
    aws_ssm_parameter.ma1sd__email__host,
    aws_ssm_parameter.ma1sd__email__port,
    aws_ssm_parameter.ma1sd__email__login,
    aws_ssm_parameter.ma1sd__email__password,
  ]

  vpc_security_group_ids = [
    var.security_group_id_general,
    aws_security_group.ma1sd.id,
  ]

  iam_instance_profile = aws_iam_instance_profile.ma1sd_profile.id

  tags              = module.label_ma1sd.tags
  availability_zone = var.availability_zone_1

  user_data = <<EOF
#cloud-config

bootcmd:
  - 'while [ `lsblk | wc -l` -lt 4 ]; do echo waiting on disks...; sleep 5; done; lsblk; sleep 5; lsblk; lsblk --pairs --output NAME,TYPE,FSTYPE,LABEL /dev/nvme1n1;'

mounts:
  - [ "LABEL=ma1sd_state", "/var/lib/ma1sd" ]

disk_setup:
  /dev/nvme1n1:
    layout:
      - 100
    overwrite: false
    table_type: 'gpt'

fs_setup:
  -   device: /dev/nvme1n1
      partition: any
      label: ma1sd_state
      filesystem: ext4
      overwrite: false

runcmd:
  - 'chown -R ma1sd:ma1sd /var/lib/ma1sd'

EOF

}

resource "aws_security_group" "ma1sd" {
  name   = module.label_ma1sd.id
  vpc_id = var.vpc_id

  ingress {
    from_port   = 8090
    to_port     = 8090
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = var.node_exporter_port
    to_port     = var.node_exporter_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label_ma1sd.tags
}

resource "aws_volume_attachment" "ma1sd_state" {
  device_name  = "/dev/sdh"
  volume_id    = aws_ebs_volume.ma1sd_state.id
  instance_id  = aws_instance.ma1sd.id
  skip_destroy = true
}

resource "aws_ebs_volume" "ma1sd_state" {
  availability_zone = var.availability_zone_1
  size              = 2
  encrypted         = true
  kms_key_id        = module.kms_key_ma1sd.key_arn

  tags = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__hostname" {
  name      = "${local.ssm_prefix_ma1sd}/hostname"
  value     = var.ma1sd_hostname
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__homeserver_internal_uri" {
  name      = "${local.ssm_prefix_ma1sd}/homeserver_internal_uri"
  value     = "http://${aws_instance.synapse.private_ip}:${local.synapse_client_port}"
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__endpoint" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db/endpoint"
  value     = var.database_endpoint
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__name" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db/name"
  value     = var.database_name
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__username" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db/username"
  value     = var.database_username
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__password" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db/password"
  value     = var.database_password
  type      = "SecureString"
  key_id    = module.kms_key_ma1sd.key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__email__from" {
  name      = "${local.ssm_prefix_ma1sd}/email/from"
  value     = var.smtp_from
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__email__host" {
  name      = "${local.ssm_prefix_ma1sd}/email/host"
  value     = var.smtp_host
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__email__port" {
  name      = "${local.ssm_prefix_ma1sd}/email/port"
  value     = var.smtp_port
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__email__login" {
  name      = "${local.ssm_prefix_ma1sd}/email/login"
  value     = var.smtp_login
  type      = "String"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__email__password" {
  name      = "${local.ssm_prefix_ma1sd}/email/password"
  value     = var.smtp_password
  type      = "SecureString"
  key_id    = module.kms_key_ma1sd.key_arn
  overwrite = true
}
