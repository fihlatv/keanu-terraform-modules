variable "region" {
  type        = "string"
  description = "AWS Region this session manager config is for"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = "string"
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = "string"
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = "string"
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "tag_key" {
  description = "The tag key on EC2 instances that users will be able to access via session manager"
  type        = "string"
}

variable "tag_value" {
  description = "The tag value on EC2 instances that users will be able to access via session manager"
  type        = "string"
}

variable "kms_key_arn" {
  description = "The ARN of a CMK; enables the creation of a data encryption key that will be used to encrypt session data"
  type        = "string"
}
