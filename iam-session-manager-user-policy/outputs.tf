output "user_session_manager_full_admin_policy_arn" {
  description = "ARN of the created policy that allows users full access to all session manager sessions"
  value       = "${aws_iam_policy.user_session_manager_full_admin.arn}"
}

output "user_session_manager_tagged_instances_policy_arn" {
  description = "ARN of the created policy that allows users access to manage their sessions on instances tagged with ${var.tag_key}=${var.tag_value}"
  value       = "${aws_iam_policy.user_session_manager_tagged_instances.arn}"
}
