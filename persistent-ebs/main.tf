/**
 * ## Persistent Data on EBS
 *
 * This module provides an EBS volume and associated IAM profile/role to be
 * used with an EC2 instance or auto-scaling group. This module is best when
 * used in conjunction with a single-node auto-scaling group, and with the
 * init-snippet that attaches the named EBS volume on boot.
 *
 */

module "label" {
  source      = "../null-label"
  namespace   = "${var.namespace}"
  name        = "${var.name}"
  environment = "${var.environment}"
  delimiter   = "${var.delimiter}"
  attributes  = "${var.attributes}"
  tags        = "${merge(var.tags, map("AZ", var.az))}"
}

resource "aws_ebs_volume" "main" {
  availability_zone = "${var.az}"
  size              = "${var.size}"
  type              = "${var.volume_type}"
  encrypted         = "${var.encrypted}"
  kms_key_id        = "${var.kms_key_id}"
  snapshot_id       = "${var.snapshot_id}"
  tags              = "${var.tags}"
}
