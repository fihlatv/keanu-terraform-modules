## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| bastion\_ami | The ami id that the basation instance will use | string | n/a | yes |
| cloudflare\_zone | The cloudflare zone to add the dns record to (e.g., `keanu.im`) | string | n/a | yes |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| domain\_name | The full domain name to create in the cloudflare zone (e.g, `neo.keanu.im`) | string | n/a | yes |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| public\_keys\_bucket | The S3 bucket name from which ssh keys will be fetched | string | n/a | yes |
| region | The region where the bastion is to be placed | string | n/a | yes |
| ssh\_user | The ssh user used in the ami | string | `"admin"` | no |
| subnet\_ids |  | list | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |
| vpc\_id |  | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| asg\_id |  |
| bastion\_host |  |
| bastion\_public\_ip |  |
| security\_group\_id |  |
| ssh\_user |  |

