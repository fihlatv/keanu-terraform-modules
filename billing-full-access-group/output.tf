output "group_name" {
  value       = join("", aws_iam_group.default.*.name)
  description = "The Group's name"
}

output "group_id" {
  value       = join("", aws_iam_group.default.*.id)
  description = "The Group's ID"
}

output "group_unique_id" {
  value       = join("", aws_iam_group.default.*.unique_id)
  description = "Group's unique ID assigned by AWS"
}

output "group_arn" {
  value       = join("", aws_iam_group.default.*.arn)
  description = "The ARN assigned by AWS for the Group"
}

output "policy_name" {
  value       = aws_iam_group_policy.billing_full_access.name
  description = "The name of the policy"
}

output "policy_id" {
  value       = aws_iam_group_policy.billing_full_access.id
  description = "The policy ID"
}
