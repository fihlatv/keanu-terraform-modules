# Create a RDS security group in the VPC which our database will belong to.

module "label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = var.attributes
  tags        = var.tags
}

module "kms_key_rds" {
  source      = "../kms-key"
  name        = var.name
  namespace   = var.namespace
  environment = var.environment
  tags        = module.label.tags
  attributes  = ["rds"]

  description             = "KMS key for rds"
  deletion_window_in_days = 10
  enable_key_rotation     = "true"
  alias                   = "alias/${module.label.id}_kms_key"
}

resource "aws_security_group" "rds" {
  vpc_id = var.vpc_id

  # Keep the instance private by only allowing traffic from the web server.
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.label.tags
}

module "db" {
  source                  = "terraform-aws-modules/rds/aws"
  version                 = "~> v2.0"
  identifier              = module.label.id
  engine                  = "postgres"
  family                  = var.family
  engine_version          = var.engine_version
  major_engine_version    = var.major_engine_version
  instance_class          = var.instance_class
  allocated_storage       = var.allocated_storage
  storage_encrypted       = true
  kms_key_id              = module.kms_key_rds.key_arn
  name                    = var.database_name
  username                = var.database_username
  password                = var.database_password
  port                    = "5432"
  subnet_ids              = var.subnet_ids
  vpc_security_group_ids  = [aws_security_group.rds.id]
  multi_az                = false
  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = var.backup_retention_period
  create_db_option_group  = false

  create_monitoring_role = true
  monitoring_interval    = "60"
  monitoring_role_name   = "AllowRDSMonitoringFor-${module.label.id}"

  # Snapshot name upon DB deletion
  final_snapshot_identifier   = "${module.label.id}-final-snapshot"
  deletion_protection         = var.deletion_protection_enabled
  skip_final_snapshot         = var.skip_final_snapshot
  allow_major_version_upgrade = var.allow_major_version_upgrade
  apply_immediately           = var.apply_immediately
  tags                        = module.label.tags
}
